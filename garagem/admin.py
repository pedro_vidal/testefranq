from django.contrib import admin
from .models import Garagem, Veiculos


class GaragemAdmin(admin.ModelAdmin):
    list_display = ('id', 'phone', 'email')


admin.site.register(Garagem, GaragemAdmin)


class VeiculosAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'cor', 'ano', 'id_garagem', 'moto')


admin.site.register(Veiculos, VeiculosAdmin)