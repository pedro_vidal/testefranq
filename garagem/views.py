from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from .models import Veiculos, Garagem
from rest_framework import generics
from .serializers import GarageSerializer,VeiculosSerializer

from .forms import CreateUserForm


def main(request): # homepage
    if request.user.is_anonymous:
        return render(request, 'garage/main.html')
    else:
        glist = []
        garagens = Garagem.objects.filter(phone=request.user.phone)
        for garagem in garagens:
            glist.append(garagem.id)
        veiculos = Veiculos.objects.filter(id_garagem__in=glist)
        return render(request, 'garage/main.html', {'veiculos': veiculos})


def new(request): # view veiculo1
    garagens = Garagem.objects.filter(phone=request.user.phone)
    return render(request, 'garage/newVehicle.html', {'garagens': garagens})


def car(request, car): # view informação dos carros
    if request.user.is_anonymous:
        return render(request, 'garage/main.html')
    else:
        glist = []
        vlist = []
        garagens = Garagem.objects.filter(phone=request.user.phone)
        for garagem in garagens:
            glist.append(garagem.id)
        for i in glist:
            veiculos = Veiculos.objects.filter(id_garagem=i)
            for veiculo in veiculos:
                if veiculo.name == car:
                    vlist.append(veiculo)
        return render(request, 'garage/car.html', {'veiculos': vlist})

def newg(request): # view garagem1
    return render(request, 'garage/newGarage.html')


def news(request): # view veiculos2
    garagens = Garagem.objects.filter(phone=request.user.phone)
    modelo = request.POST.get("carModel")
    cor = request.POST.get("carCor")
    ano = request.POST.get("carAno")
    gar = request.POST.get("gar")
    moto = request.POST.get("moto")
    if moto == "on":
        moto = True
    else:
        moto = False
    veic = Veiculos(name=modelo, cor=cor, ano=ano, id_garagem=gar, moto=moto)
    veic.save()
    return render(request, 'garage/newVehicle.html', {"message":"Veículo registrado com sucesso!", 'garagens': garagens})


def newgs(request): # view garagem 2
    gar = Garagem(email=request.user.email, phone=request.user.phone)
    gar.save()
    return render(request, 'garage/newGarage.html', {"message": "Garage adiquirida com sucesso!"})


def register(request): # view de registro
    if request.method == 'POST':
        form = CreateUserForm(request.POST)

        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            phone = form.cleaned_data['phone']
            email = form.cleaned_data['email']
            user = authenticate(username=username, password=password)
            login(request, user)
            garage = Garagem(phone=phone, email=email) # cria uma garagem para o usuário cadastrado automaticamente
            garage.save()
            return redirect(main)
    else:
        form = CreateUserForm()
    context = {'form': form}
    return render(request, 'registration/register.html', context)


class apiGarage(generics.ListAPIView): # View API Garagens

    queryset = Garagem.objects.all()
    serializer_class = GarageSerializer

class apiVeiculos(generics.ListAPIView): # View API Veiculos

    queryset = Veiculos.objects.all()
    serializer_class = VeiculosSerializer