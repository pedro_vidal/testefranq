from django.db import models


class Garagem(models.Model):
    phone = models.CharField(max_length=255)
    email = models.CharField(max_length=255)

    def __str__(self):
        return self.email


class Veiculos(models.Model):
    name = models.CharField(max_length=255)
    cor = models.CharField(max_length=255)
    ano = models.CharField(max_length=255)
    moto = models.BooleanField(default=False)
    id_garagem = models.IntegerField()

    def __str__(self):
        return self.id_garagem